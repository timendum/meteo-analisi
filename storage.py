"""Store and retrieve all data"""
from functools import lru_cache
from typing import Dict, List

import gspread
from gspread.utils import rowcol_to_a1

import config


class Storage(object):
    """Store and retrieve all data"""

    __col_config = None

    def __init__(self):
        self.gsclient = Storage._get_gsclient()
        self.results = self.gsclient.open_by_key(config.GSHEET_RESULTS)

    @staticmethod
    def _get_gsclient() -> gspread.Client:
        import os

        from oauth2client.service_account import ServiceAccountCredentials

        creds_filename = os.path.join(os.path.dirname(__file__), "creds.json")
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            creds_filename, ["https://spreadsheets.google.com/feeds"]
        )
        return gspread.authorize(credentials)

    @property
    def col_config(self) -> Dict[str, str]:
        """Retrive column configuration"""
        if not self.__col_config:
            sheet = self.results.worksheet("config")
            records = sheet.get_all_records()
            col_config = {}
            for record in records:
                col_config[record["nome"]] = record["colonna"]
            self.__col_config = col_config
        return self.__col_config

    @lru_cache()
    def __row_day(self, day) -> int:
        """Retrive row for the day"""
        shgiorni = self.results.worksheet("giorni")
        records = shgiorni.get_all_records()
        for idx, record in enumerate(records):
            if record["giorno"] == day:
                # row starts from 1, + 1 for header
                # idx | label      | row number
                # n/a | giorno     | 1
                # 0   | 20/07/2017 | 2
                # so add 2
                return idx + 2
        # new day, insert in sheets
        shgiorni.insert_row([day], index=2)
        self.results.worksheet("previsione").insert_row([day], index=2)
        self.results.worksheet("risultati").insert_row([day], index=2)
        return 2

    def store_meteo(self, day, results) -> None:
        """Store results"""
        sheet = self.results.worksheet("previsione")
        for extractor, weathers in results.items():
            if not weathers.get("w"):
                continue
            rownum = self.__row_day(day)
            colnum = self.col_config[extractor]
            cell_list = Storage.__range_rowcol(sheet, rownum, colnum)
            for i in range(0, 3):
                cell_list[i].value = weathers["w"][i]
            sheet.update_cells(cell_list)

    @staticmethod
    def __range_rowcol(sheet, rownum, colnum) -> List[gspread.Cell]:
        """Return a cell list from rownum:colnum to rownum:colnum+2 """
        cell_list = sheet.range(
            "%s:%s" % (rowcol_to_a1(rownum, colnum), rowcol_to_a1(rownum, colnum + 2))
        )
        return cell_list

    def store_survey(self, day, url) -> None:
        """Store survey url"""
        sheet = self.results.worksheet("previsione")
        sheet.update_cell(self.__row_day(day), self.col_config["survey"], url)

    def store_actuals(self, day, results, votes=-1) -> None:
        """Store results"""
        sheet = self.results.worksheet("risultati")
        rownum = self.__row_day(day)
        colnum = self.col_config["risultati"]
        sheet.update_cell(rownum, self.col_config["voti"], votes)
        cell_list = Storage.__range_rowcol(sheet, rownum, colnum)
        for i in range(0, 3):
            cell_list[i].value = results[i]
        sheet.update_cells(cell_list)

    def retrieve_survey_url(self, day) -> str:
        """Retrieve survery urls"""
        sheet = self.results.worksheet("previsione")
        return sheet.cell(self.__row_day(day), self.col_config["survey"]).value

    def retrieve_mails(self) -> List[str]:
        """Retrieve mails"""
        sheet = self.gsclient.open_by_key(config.GSHEET_EMAIL).get_worksheet(0)
        emails = sheet.col_values(1)
        return [email for email in emails if email]

    def check_day(self, day) -> bool:
        """Check if day is the last day"""
        shgiorni = self.results.worksheet("giorni")
        last_day = shgiorni.cell(2, 1).value
        return day == last_day

    def retrieve_meteo(self, day) -> List[List[int]]:
        """Retrieve results [ morning=[provider1, provider2,...] , afternoon, evening]"""
        sheet = self.results.worksheet("previsione")
        row = sheet.row_values(self.__row_day(day))
        # skip first column, with day string
        row = row[2:]
        # skip blank rows
        row = [x for x in row if x]
        stati = []
        for i in range(0, 3):
            stati.append([int(row[n]) for n in range(i, len(row), 3)])
        return stati


if __name__ == "__main__":

    def main():
        """For testing"""
        test = Storage()
        print(test.col_config)
        print(test.retrieve_mails())
        print(test.retrieve_survey_url("21/07/2017"))
        print(test.retrieve_meteo("04/09/2017"))
        print("Day %s" % test.check_day("02/08/2017"))

    main()
