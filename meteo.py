"""Fetch meteo data from multiple source"""
import re
from statistics import StatisticsError, mode, pvariance
from typing import Dict, List

import requests
from bs4 import BeautifulSoup

import config

HTML_PARSER = "html.parser"

W_VARIANCE_LIMIT = config.W_VARIANCE_LIMIT  # 0.5

SCONOSCIUTO = 0
NON_PIOVE = 1
DEBOLE = 2
PIOGGIA = 3
FORTE = 4
NEVE = 5

MOMENTI = [
    "Tra le 7:00 e le 9:00",
    "Tra le 12:00 e le 14:00",
    "Tra le 18:00 e le 19:00",
]

STATI = [
    "Sconosciuto",
    "Sereno o nuvoloso",
    "Pioggia debole",
    "Pioggia",
    "Temporale",
    "Neve",
]


def text_to_perc(perc_string) -> float:
    """Convert '62%' to 0.62"""
    try:
        return int(perc_string.replace("%", "").strip()) / 100
    except ValueError:
        return 0


def states_to_strings(states) -> List[str]:
    """Returns the text corresponding to the numeric state"""
    return [STATI[i] for i in states]


def map_re_states(origins, states_map) -> List[int]:
    """Perform the map between an array of string and a dict of (re, final_string)"""
    for state_map in states_map:
        state_map["reg"] = re.compile(state_map["re"], re.I)

    mapped = []
    for origin in origins:
        for state_map in states_map:
            if state_map["reg"].search(origin):
                mapped.append(state_map["dest"])
                break
        else:
            import inspect

            print("Stato sconosciuto: %s (%s)" % (origin, inspect.stack()[1][1]))
            mapped.append(SCONOSCIUTO)
    return mapped


def hours_to_moments(hours) -> List[int]:
    """Return three moments from hours"""
    moments = [0, 0, 0]
    for i in range(0, 3):
        if SCONOSCIUTO in hours[i * 2 : i * 2 + 2]:
            moments[i] = SCONOSCIUTO
        else:
            moments[i] = max(hours[i * 2 : i * 2 + 2])
    return moments


def fetch_json(url):
    """Get a json response from url"""
    req = requests.get(url)
    if req.status_code != 200:
        raise ValueError("HTTP status %d for %s" % (req.status_code, url))
    return req.json()


def fetch_soup(url, headers=None, cookies=None):
    """Get a BeautifulSoup response from url"""
    rheaders = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/80.0",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    }
    if headers:
        rheaders.update(headers)
    rcookies = dict()
    if cookies:
        rcookies.update(cookies)
    req = requests.get(url, headers=rheaders, cookies=rcookies)
    if req.status_code != 200:
        raise ValueError("HTTP status %d for %s" % (req.status_code, url))
    return BeautifulSoup(req.text, "html5lib")


class Meteo(object):
    """Extract all meteo data"""

    __results: Dict[str, Dict[str, int]] = {}
    __variance: Dict[str, int] = {}

    @property
    def results(self):
        """Invoke all the extractors"""
        if not self.__results:
            self.__bmeteo()
            self.__yrno()
            self.__meteoam()
            self.__meteoit()
            self.__meteoblue()
        return self.__results

    @property
    def variance(self):
        """Calculate variances"""
        if not self.__variance:
            variance = []
            for i in range(0, 3):
                variance.append(
                    pvariance(
                        [value["w"][i] for _, value in self.results.items() if value.get("w")]
                    )
                )
            self.__variance["w"] = variance
        return self.__variance

    def check_results(self):
        """Check if the results have low variance"""
        w_variance = sum(self.variance["w"]) / len(self.variance["w"])
        if w_variance > W_VARIANCE_LIMIT:
            return True
        for i in range(0, 3):
            try:
                mode([self.results[k]["w"][i] for k in self.results.keys()])
            except StatisticsError:
                return True
        return False

    def __meteoit(self):
        """Invoke MeteoIt extractor"""
        from meteoit import MeteoIt

        extractor = MeteoIt()
        try:
            self.__results["MeteoIt"] = {"w": extractor.weather}
        except Exception as e:
            print("MeteoIt", e)
            self.__results["MeteoIt"] = {"w": (SCONOSCIUTO, SCONOSCIUTO, SCONOSCIUTO)}

    def __darksky(self):
        """Invoke DarkSky extractor"""
        from darksky import DarkSky

        extractor = DarkSky()
        try:
            self.__results["DarkSky"] = {"w": extractor.weather}
        except Exception as e:
            print("DarkSky", e)
            self.__results["DarkSky"] = {"w": (SCONOSCIUTO, SCONOSCIUTO, SCONOSCIUTO)}

    def __meteoam(self):
        """Invoke MeteoAM extractor"""
        from meteoam import MeteoAM

        extractor = MeteoAM()
        try:
            self.__results["MeteoAM"] = {"w": extractor.weather}
        except Exception as e:
            print("MeteoAM", e)
            self.__results["MeteoAM"] = {"w": (SCONOSCIUTO, SCONOSCIUTO, SCONOSCIUTO)}

    def __bmeteo(self):
        """Invoke 3BMeteo extractor"""
        from bmeteo import BMeteo

        extractor = BMeteo()
        try:
            self.__results["3BMeteo"] = {"w": extractor.weather}
        except Exception as e:
            print("3BMeteo", e)
            self.__results["3BMeteo"] = {"w": (SCONOSCIUTO, SCONOSCIUTO, SCONOSCIUTO)}

    def __meteoblue(self):
        """Invoke MeteoBlue extractor"""
        from meteoblue import MeteoBlue

        extractor = MeteoBlue()
        try:
            self.__results["MeteoBlue"] = {"w": extractor.weather}
        except Exception as e:
            print("MeteoBlue", e)
            self.__results["MeteoBlue"] = {"w": (SCONOSCIUTO, SCONOSCIUTO, SCONOSCIUTO)}

    def __yrno(self):
        """Invoke YrNo extractor"""
        from yrno import YrNo

        extractor = YrNo()
        try:
            self.__results["YrNo"] = {"w": extractor.weather}
        except Exception as e:
            print("YrNo", e)
            self.__results["YrNo"] = {"w": (SCONOSCIUTO, SCONOSCIUTO, SCONOSCIUTO)}

    @property
    def distinct_weathers(self):
        """Return all the distinct weather arrays from all the extractors"""
        w_set = set()
        for _, extracted in self.results.items():
            if extracted.get("w"):
                w_set.add(tuple(extracted["w"]))
        # convert from the list the elements back to list from touple and return a list
        return [list(element) for element in w_set]


def main():
    """Run project"""
    meteo = Meteo()
    for key, values in meteo.results.items():
        print("{}: {}".format(key, str(states_to_strings(values["w"]))))
    print("Varianze: ")
    print(meteo.variance["w"])
    print(sum(meteo.variance["w"]) / len(meteo.variance["w"]))
    print(meteo.check_results())
    print("Distincts: " + str(meteo.distinct_weathers))


if __name__ == "__main__":
    main()
