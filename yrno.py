"""3BMeteo extractor"""
from typing import Dict, List

import meteo

HOURS = ("07", "08", "12", "13", "18", "19")


class YrNo:
    """Extract 7-8 -> Mattina, 12-13 -> Pomeriggio, 18-19 -> Sera.
    Source: https://www.yr.no/en/forecast/hourly-table/2-3173435/Italy/Lombardy/Metropolitan%20City%20of%20Milan/Milan?i=1"""

    results: Dict[str, List[int]] = {}

    @staticmethod
    def map_states(day) -> List[int]:
        """Convert site states to generic states"""
        # fmt: off
        states_maps = [
            {'re': 'snow',            'dest': meteo.NEVE},       # noqa: E241
            {'re': 'thunderstorm',    'dest': meteo.FORTE},      # noqa: E241
            {'re': 'heavy rain',      'dest': meteo.FORTE},      # noqa: E241
            {'re': 'heavy sleet',     'dest': meteo.FORTE},      # noqa: E241
            {'re': 'light rain',      'dest': meteo.DEBOLE},     # noqa: E241
            {'re': 'light sleet',     'dest': meteo.DEBOLE},     # noqa: E241
            {'re': 'rain',            'dest': meteo.PIOGGIA},    # noqa: E241
            {'re': 'sleet',           'dest': meteo.PIOGGIA},    # noqa: E241
            {'re': 'fair',            'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'clear',           'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'cloudy',          'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'fog',             'dest': meteo.NON_PIOVE},  # noqa: E241
        ]
        # fmt: on
        states = meteo.map_re_states(day, states_maps)
        return states

    @property
    def weather(self) -> List[int]:
        """Extract weather"""
        if not self.results:
            self.extract()
        return self.results["w"]

    def extract(self) -> None:
        """Extract data"""
        soup = meteo.fetch_soup(
            "https://www.yr.no/en/forecast/hourly-table/2-3173435/Italy/Lombardy/Metropolitan%20City%20of%20Milan/Milan?i=1"
        )
        # weather
        table = soup.find(class_="hourly-weather-table")
        rows = table.find_all(class_="fluid-table__row")
        moments = []
        for row in rows:
            if row.find("time").text.strip() in HOURS:
                moments.append(row.find(class_="symbol").text)
        if len(moments) != len(HOURS):
            raise ValueError("YrNo: non trovati MOMENTS")
        imoments = self.map_states(moments)
        self.results["w"] = meteo.hours_to_moments(imoments)


if __name__ == "__main__":

    def main() -> None:
        """For testing"""
        extractor = YrNo()
        print("YrNo:", end="")
        print(" - ".join(meteo.states_to_strings(extractor.weather)))

    main()
