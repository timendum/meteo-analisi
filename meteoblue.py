"""meteo.it extractor"""
from typing import Dict, List

import meteo

HOURS = [6, 7, 11, 12, 17, 18]  # icons starts at 1:00


class MeteoBlue(object):
    """Extract 7-8 -> Mattina, 12-13 -> Pomeriggio, 18-19 -> Sera.
    Source: https://www.meteoblue.com/en/weather/week/milan_italy_3173435?day=2
            > More"""

    results: Dict[str, List[int]] = {}

    @staticmethod
    def map_states(day) -> List[int]:
        """Convert site states to generic states"""
        # fmt: off
        states_maps = [
            {'re': 'mixed',            'dest': meteo.NON_PIOVE},   # noqa: E241
            {'re': 'snow',             'dest': meteo.NEVE},        # noqa: E241
            {'re': 'thunderstorm',     'dest': meteo.FORTE},       # noqa: E241
            {'re': 'heavy rain',       'dest': meteo.DEBOLE},      # noqa: E241
            {'re': 'temporale',        'dest': meteo.FORTE},       # noqa: E241
            {'re': 'light rain',       'dest': meteo.DEBOLE},      # noqa: E241
            {'re': 'rain',             'dest': meteo.PIOGGIA},     # noqa: E241
            {'re': 'fog',              'dest': meteo.NON_PIOVE},   # noqa: E241
            {'re': 'clear',            'dest': meteo.NON_PIOVE},   # noqa: E241
            {'re': 'cloudy',           'dest': meteo.NON_PIOVE},   # noqa: E241
            {'re': 'overcast',         'dest': meteo.NON_PIOVE}    # noqa: E241
        ]
        # fmt: on
        states = meteo.map_re_states(day, states_maps)
        return states

    @property
    def weather(self) -> List[int]:
        """Extract weather"""
        if not self.results:
            self.extract()
        return self.results["w"]

    def extract(self) -> None:
        """Extract data"""
        soup = meteo.fetch_soup(
            "https://www.meteoblue.com/en/weather/week/milan_italy_3173435?day=2",
            cookies={"addparam": "true"},
        )
        pictos = soup.find(class_="pictos_1h")
        if not pictos:
            raise ValueError("MeteoBlue: non trovato pictos_1h")
        icons = pictos.find_all(class_="picon")
        if not icons:
            raise ValueError("MeteoBlue: non trovato icons")
        # weather
        hours = []
        for hour in HOURS:
            hours.append(icons[hour]["title"])
        hours = self.map_states(hours)
        self.results["w"] = meteo.hours_to_moments(hours)


if __name__ == "__main__":

    def main():
        """For testing"""
        extractor = MeteoBlue()
        print("MeteoBlue:")
        print(str(meteo.states_to_strings(extractor.weather)))

    main()
