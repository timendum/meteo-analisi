"""Send notification mail"""
from datetime import date

import requests

import config

__DATE_FORMAT = "%d/%m/%Y"


def send_message(tos, textes) -> bool:
    """Send a mail"""
    textes = ["Ciao,"] + textes + ["Saluti,\nTimendum"]
    req = requests.post(
        "%s/messages" % config.MAILGUN_URL,
        auth=("api", config.MAILGUN_KEY),
        data={
            "from": config.MAIL_FROM,
            "to": tos,
            "subject": "Meteo a Milano - %s" % date.today().strftime(__DATE_FORMAT),
            "text": "\n\n".join(textes),
        },
    )
    return req.status_code == 200


def text_surveys(day, url) -> None:
    """Compose the text for mailing the surveys"""
    return (
        """Vorrei controllare le previsioni, puoi farmi sapere com'è andato oggi ("""
        + day
        + """) il tempo?
Rispondi a questo sondaggio: """
        + url
    )


def text_rain(labels, states) -> str:
    """Compose the text for mailing the surveys"""
    text = "Per domani è previsto:"
    for label, state in zip(labels, states):
        if state:
            text += "\n- Domani %s: %s" % (label, state)
    return text


if __name__ == "__main__":

    def main():
        """For testing"""
        text = [
            text_surveys("10/09/2017", "https://reddit.com/r/italy"),
            text_rain(["notte", "mai", "tarti"], ["pieno", None, "Vuoto"]),
        ]
        # print(send_message([config.MAIL_FROM], text))
        print("\n\n".join(text))

    main()
