"""MeteoAm extractor"""
from typing import Dict, List

import meteo

HOURS = ("07:00", "08:00", "13:00", "14:00", "19:00", "20:00")


class MeteoAM(object):
    """Extract 7,8 -> Mattina, 13,14 -> Pomeriggio, 19,20 -> Sera.
    Source: http://www.meteoam.it/ta/previsione/87/milano > DOMANI"""

    results: Dict[str, List[int]] = {}
    NAME = "MeteoAM"

    @staticmethod
    def map_states(day) -> List[int]:
        """Convert site states to generic states"""
        # fmt: off
        states_maps = [
            {'re': 'neve',             'dest': meteo.NEVE},       # noqa: E241
            {'re': 'temporale',        'dest': meteo.FORTE},      # noqa: E241
            {'re': 'pioggia debole',   'dest': meteo.DEBOLE},     # noqa: E241
            {'re': 'pioggia moderata', 'dest': meteo.PIOGGIA},    # noqa: E241
            {'re': 'nebbia',           'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'nuvoloso',         'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'sereno',           'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'coperto',          'dest': meteo.NON_PIOVE}   # noqa: E241
        ]
        # fmt: on
        states = meteo.map_re_states(day, states_maps)
        return states

    @staticmethod
    def find_weather(soup):
        """Find weather informations"""
        domani = soup.find(id="domani")
        if not domani:
            raise ValueError("AmMeteo: non trovato domani")
        rows = domani.find_all("tr")
        if not rows:
            raise ValueError("AmMeteo: non trovato rows")
        frows = []
        for row in rows:
            if row.find("th") and row.find("th").text.strip() in HOURS:
                frows.append(row)
        if len(frows) != 3:
            raise ValueError("AmMeteo:rows non corrette")
        return frows

    @property
    def weather(self) -> List[int]:
        """Extract weather"""
        if not self.results:
            self.extract()
        return self.results["w"]

    def extract(self) -> None:
        """Extract data"""
        soup = meteo.fetch_soup("http://www.meteoam.it/ta/previsione/87/milano")
        rows = MeteoAM.find_weather(soup)
        weather = [None] * 3
        for index, row in enumerate(rows):
            weather[index] = row.find("img")["title"]

        self.results["w"] = MeteoAM.map_states(weather)


if __name__ == "__main__":

    def main():
        """For testing"""
        extractor = MeteoAM()
        extractor.extract()
        print("MeteoAM: ", end="")
        print(" - ".join(meteo.states_to_strings(extractor.weather)))

    main()
