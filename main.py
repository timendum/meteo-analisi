"""Main project file"""
import traceback
from datetime import date, timedelta
from statistics import StatisticsError, mode

import mail
import meteo
import survey
from storage import Storage

__DATE_FORMAT = "%d/%m/%Y"
STORE = Storage()
TOMORROW = (date.today() + timedelta(days=1)).strftime(__DATE_FORMAT)
TODAY = date.today().strftime(__DATE_FORMAT)
YESTERDAY = (date.today() + timedelta(days=-1)).strftime(__DATE_FORMAT)


def check_tomorrow(day=None):
    """Tomorrow: Check and store meteo and surveys (if needed)"""
    if not day and STORE.check_day(TOMORROW):
        # tomorrow already insered
        return
    day = day or TOMORROW
    weather = meteo.Meteo()
    results = weather.results
    STORE.store_meteo(day, results)
    if weather.check_results() and len(weather.distinct_weathers) > 1:
        numeric_states = weather.distinct_weathers
        text_states = []
        for states in numeric_states:
            text_states.append(
                "; ".join(
                    [
                        "%s: %s" % (momento, meteo.STATI[state])
                        for momento, state in zip(meteo.MOMENTI, states)
                    ]
                )
            )
        text_states.append("Nessuna delle precedenti")
        url = survey.create(day, text_states)
        STORE.store_survey(day, url)
    else:
        states = [[], [], []]
        for weathers in results.values():
            if not weathers.get("w"):
                continue
            for i in range(0, 3):
                states[i].append(weathers["w"][i])
        states = [__mode_or_default(state, 0) for state in states]
        STORE.store_actuals(day, states)


def _survey_mail(day=None):
    """Compose mail content for the survey"""
    day = day or TODAY
    url = STORE.retrieve_survey_url(day)
    mail_body = None
    if url:
        mail_body = mail.text_surveys(day, url)
    return mail_body


def __mode_or_default(elements, default):
    """Return the mode of the list or default if mode fails"""
    try:
        return mode(elements)
    except StatisticsError:
        return default


def _rain_mail(day=None, force=False):
    """Compose mail content for the rain alert"""
    day = day or TOMORROW
    states = [__mode_or_default(state, meteo.PIOGGIA) for state in STORE.retrieve_meteo(day)]
    if not force:
        for state in states:
            if state not in [meteo.SCONOSCIUTO, meteo.NON_PIOVE]:
                force = True
                break
    if force:
        return mail.text_rain(meteo.MOMENTI, [meteo.STATI[state] for state in states])
    return None


def send_mail():
    """Today: Send Mail if needed"""
    mail_body = [_survey_mail()]
    mail_body.append(_rain_mail(day=None, force=mail_body[0]))
    mail_body = [item for item in mail_body if item]
    if mail_body:
        tos = STORE.retrieve_mails()
        mail.send_message(tos, mail_body)


def update_votes(day=None):
    """Yesterday: Retrive and store votes"""
    day = day or YESTERDAY
    urls = STORE.retrieve_survey_url(day)
    if urls:
        pool = survey.votes(urls)
        votes = sum([option[0] for option in pool])
        most_votes = pool[0][0]
        if votes == 0 or ";" not in pool[0][1]:
            STORE.store_actuals(day, [meteo.SCONOSCIUTO, meteo.SCONOSCIUTO, meteo.SCONOSCIUTO], 0)
            return
        # String to array -- From "Mattino;Pomeriggio;Sera" to [Mattino, Pomeriggio, Sera]
        pool_results = [option[1].split("; ") for option in pool if option[0] == most_votes]
        # Clear string -- "Mattino: Sereno o nuvoloso" to "Sereno o nuvoloso"
        pool_results = [[sector.split(": ")[1] for sector in result] for result in pool_results]
        if len(pool_results) == 1:
            most_voted = pool_results[0]
        else:
            most_voted = []
            for i in range(0, 3):
                most_voted.append(
                    __mode_or_default([a[i] for a in pool_results], meteo.STATI[meteo.SCONOSCIUTO])
                )
        numerics = []
        for i in range(0, 3):
            numerics.append(meteo.STATI.index(most_voted[i]))
        STORE.store_actuals(day, numerics, most_votes / votes)


if __name__ == "__main__":
    # pylint: disable=W0703
    try:
        update_votes()
    except Exception as exception:
        print(exception)
        traceback.print_exc()
    try:
        check_tomorrow()
        send_mail()
    except Exception as exception:
        print(exception)
        traceback.print_exc()
