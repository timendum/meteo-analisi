"""Create and manage survey or poll for analisys"""
import json
import re
from typing import List, Tuple

import requests

ENDPOINT = "https://www.strawpoll.me/api/v2/polls"


class StrawPoll(object):
    """StrawPoll wrapper"""

    def __init__(self, poll_id=None, data=None) -> None:
        self.poll_id = poll_id
        if data:
            req = requests.post(ENDPOINT, data=json.dumps(data))
            if req.status_code != 200:
                raise ValueError("StrawPoll status %d" % (req.status_code))
            response = req.json()
            self.poll_id = response["id"]
            self.url = "http://www.strawpoll.me/%d/" % self.poll_id

    @classmethod
    def from_id(cls, poll_id) -> "StrawPoll":
        """Create a StrawPoll object from id"""
        return cls(poll_id=poll_id)

    @classmethod
    def from_url(cls, url) -> "StrawPoll":
        """Create a StrawPoll object from url"""
        match = re.match(r"^https?://www.strawpoll.me/([0-9]+)/?", url)
        if not match:
            raise ValueError("Url not valid: %s" % url)
        poll_id = match.group(1)
        return cls(poll_id=poll_id)

    @classmethod
    def create(cls, data) -> "StrawPoll":
        """Create a StrawPoll object from data"""
        return cls(data=data)

    @property
    def votes(self) -> List[Tuple[int, str]]:
        """Fetch votes"""
        url = ENDPOINT + "/" + str(self.poll_id)
        req = requests.get(url)
        if req.status_code != 200:
            raise ValueError("StrawPoll status %d for %s" % (req.status_code, url))
        surv = req.json()
        result = [(surv["votes"][i], surv["options"][i]) for i in range(0, len(surv["votes"]))]
        result.sort(key=lambda s: s[0], reverse=True)
        return result


def create(day, states) -> str:
    """Create a survey, return the url"""
    data = {
        "title": "Che tempo ha fatto oggi (%s)" % (day),
        "options": states,
        "multi": False,
        "dupcheck": "permissive",
        "captcha": False,
    }
    poll = StrawPoll.create(data)
    return poll.url


def votes(url):
    """Returns the votes form a previous survey"""
    poll = StrawPoll.from_url(url)
    return poll.votes


def main() -> None:
    """Run test"""
    poll_votes = votes("https://www.strawpoll.me/11522383/")
    print(poll_votes)
    new_poll = create("01/01/2060", ["a", "b", "c"])
    print(new_poll)


if __name__ == "__main__":
    main()
