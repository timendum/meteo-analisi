"""DarkSky extractor"""
from datetime import datetime, timedelta
from typing import Dict, List

import meteo


class DarkSky(object):
    """Extract 7-8 -> Mattina, 12-13 -> Pomeriggio, 18-19 -> Sera.
    Source: API call hourly for 45.4668,9.1905"""

    results: Dict[str, List[int]] = {}

    @staticmethod
    def cal_tomorrow() -> datetime:
        """Calculate tomorrow datetime"""

        day = datetime.today()
        day = day.replace(hour=0, minute=0, second=0, microsecond=0)
        day = day + timedelta(days=1)
        return day

    @property
    def weather(self) -> List[int]:
        """Extract weather"""
        if not self.results:
            self.extract()
        return self.results["w"]

    def extract(self) -> None:
        """Extract data"""
        data = meteo.fetch_json(
            "https://api.darksky.net/forecast/" + meteo.config.DARKSKY_API + "/45.4668,9.1905"
            "?units=ca&exclude=currently,minutely,alerts,flags,daily"
        )
        day = []
        tomorrow = DarkSky.cal_tomorrow().timestamp()
        for entry in data["hourly"]["data"]:
            if entry["time"] >= tomorrow:
                day.append(entry)
        # fmt: off
        switcher = {
            "clear-day":                meteo.NON_PIOVE,  # noqa: E241
            "clear-night":              meteo.NON_PIOVE,  # noqa: E241
            "rain":                     meteo.PIOGGIA,    # noqa: E241
            "snow":                     meteo.NEVE,       # noqa: E241
            "sleet":                    meteo.NEVE,       # noqa: E241
            "wind":                     meteo.NON_PIOVE,  # noqa: E241
            "fog":                      meteo.NON_PIOVE,  # noqa: E241
            "cloudy":                   meteo.NON_PIOVE,  # noqa: E241
            "partly-cloudy-day":        meteo.NON_PIOVE,  # noqa: E241
            "partly-cloudy-night":      meteo.NON_PIOVE,  # noqa: E241
        }
        # fmt: on
        stati = []
        for hour in day:
            stati.append(switcher.get(hour["icon"], meteo.SCONOSCIUTO))
            # see https://en.wikipedia.org/wiki/Rain#Intensity
            if stati[-1] == meteo.PIOGGIA:
                if hour["precipIntensity"] < 2.5:
                    stati[-1] = meteo.DEBOLE
                elif hour["precipIntensity"] > 7.5:
                    stati[-1] = meteo.FORTE
        weather = []
        settori = [[7, 8], [12, 13], [18, 19]]
        for settore in settori:
            period = stati[settore[0] : settore[1]]
            average = sum(period) / len(period)
            weather.append(round(average))
        self.results["w"] = weather


if __name__ == "__main__":

    def main() -> None:
        """For testing"""
        extractor = DarkSky()
        print("DarkSky: ", end="")
        print(" - ".join(meteo.states_to_strings(extractor.weather)))

    main()
