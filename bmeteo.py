"""3BMeteo extractor"""
from typing import Dict, List, Any

import meteo

HOURS = ("07:00", "08:00", "12:00", "13:00", "18:00", "19:00")


class BMeteo:
    """Extract 7-8 -> Mattina, 12-13 -> Pomeriggio, 18-19 -> Sera.
    Source: https://www.3bmeteo.com/meteo/milano/dettagli_orari/1"""

    results: Dict[str, List[int]] = {}

    @staticmethod
    def map_states(day) -> List[int]:
        """Convert site states to generic states"""
        # fmt: off
        states_maps = [
            {'re': 'neve',             'dest': meteo.NEVE},       # noqa: E241
            {'re': 'nevischio',        'dest': meteo.NEVE},       # noqa: E241
            {'re': 'gelicidio',        'dest': meteo.NEVE},       # noqa: E241
            {'re': 'temporal',         'dest': meteo.FORTE},      # noqa: E241
            {'re': 'possibili piogge', 'dest': meteo.DEBOLE},     # noqa: E241
            {'re': 'pioviggine',       'dest': meteo.DEBOLE},     # noqa: E241
            {'re': 'pioggia debole',   'dest': meteo.DEBOLE},     # noqa: E241
            {'re': 'pioggia',          'dest': meteo.PIOGGIA},    # noqa: E241
            {'re': 'rovesci',          'dest': meteo.PIOGGIA},    # noqa: E241
            {'re': 'coperto',          'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'nubi',             'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'nuvoloso',         'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'foschia',          'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'nebbia',           'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'sereno',           'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': 'velat',            'dest': meteo.NON_PIOVE},  # noqa: E241
        ]
        # fmt: on
        states = meteo.map_re_states(day, states_maps)
        return states

    @staticmethod
    def find_weather(soup) -> List[Any]:
        """Find weather informations"""
        table = soup.find(class_="table-previsioni-ora")
        if not table:
            raise ValueError("3BMeteo: non trovata table-previsioni-ora")
        details = table.find_all(BMeteo.filter_rowtable)
        if not details:
            raise ValueError("3BMeteo: non trovati row-table")
        return details

    @staticmethod
    def filter_rowtable(tag) -> bool:
        """Filter row-table not in a row-table"""
        if not tag.has_attr("class") or "row-table" not in tag["class"]:
            return False
        for parent in tag.parents:
            if parent.has_attr("class") and "row-table" in parent["class"]:
                return False
        return True

    @property
    def weather(self) -> List[int]:
        """Extract weather"""
        if not self.results:
            self.extract()
        return self.results["w"]

    def extract(self) -> None:
        """Extract data"""
        soup = meteo.fetch_soup("https://www.3bmeteo.com/meteo/milano/dettagli_orari/1")
        # weather
        details = BMeteo.find_weather(soup)
        hours = []
        for detail in details:
            if detail.text.strip()[0:5] in HOURS:
                hours.append(detail.find("img", alt=True, src=True)["alt"])
        if len(hours) != len(HOURS):
            raise ValueError("3BMeteo: non trovati HOURS")
        hours = self.map_states(hours)
        self.results["w"] = meteo.hours_to_moments(hours)


if __name__ == "__main__":

    def main():
        """For testing"""
        extractor = BMeteo()
        print("3BMeteo:", end="")
        print(" - ".join(meteo.states_to_strings(extractor.weather)))

    main()
