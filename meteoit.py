"""meteo.it extractor"""
import json
from typing import Dict, List

import meteo

HOURS = ("07", "08", "12", "13", "18", "19")


class MeteoIt(object):
    """Extract 7-8 -> Mattina, 12-13 -> Pomeriggio, 18-19 -> Sera.
    Weather (no precipitation)
    Source: https://www.meteo.it/meteo/milano-domani-15146"""

    results: Dict[str, List[int]] = {}

    @staticmethod
    def map_states(day) -> List[int]:
        """Convert site states to generic states"""
        # prevision.enum.ts
        # fmt: off
        states_maps = [
            {'re': '^[1-6]$',     'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': '^7$',         'dest': meteo.NEVE},       # noqa: E241
            {'re': '^8$',         'dest': meteo.NEVE},       # noqa: E241
            {'re': '^9$',         'dest': meteo.NEVE},       # noqa: E241
            {'re': '1[0-1]',    'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': '12',        'dest': meteo.DEBOLE},     # noqa: E241
            {'re': '13',        'dest': meteo.PIOGGIA},    # noqa: E241
            {'re': '14',        'dest': meteo.FORTE},      # noqa: E241
            {'re': '15',        'dest': meteo.DEBOLE},     # noqa: E241
            {'re': '16',        'dest': meteo.PIOGGIA},    # noqa: E241
            {'re': '17',        'dest': meteo.FORTE},      # noqa: E241
            {'re': '18',        'dest': meteo.DEBOLE},     # noqa: E241
            {'re': '19',        'dest': meteo.PIOGGIA},    # noqa: E241
            {'re': '20',        'dest': meteo.FORTE},      # noqa: E241
            {'re': '2[1-2]',    'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': '2[3-5]',    'dest': meteo.FORTE},      # noqa: E241
            {'re': '2[6-9]',    'dest': meteo.NON_PIOVE},  # noqa: E241
            {'re': '3[0-2]',    'dest': meteo.FORTE}       # noqa: E241
        ]
        # fmt: on
        states = meteo.map_re_states(day, states_maps)
        return states

    @property
    def weather(self) -> List[int]:
        """Extract weather"""
        if not self.results:
            self.extract()
        return self.results["w"]

    def extract(self) -> None:
        """Extract data"""
        soup = meteo.fetch_soup("https://www.meteo.it/meteo/milano-domani-15146")
        dayOverview = soup.find(id="day-overview")
        jDayOverview = dayOverview["data-dayoverview"]
        orarie = json.loads(jDayOverview)['data']["hours"]
        hours = []
        for orario in orarie:
            ora = orario["time"].split("T")[1][:2]
            if ora not in HOURS:
                continue
            hours.append(str(orario["prevision"]))
        if len(hours) != len(HOURS):
            raise ValueError("MeteoIt: non trovati HOURS")
        shours = MeteoIt.map_states(hours)
        self.results["w"] = meteo.hours_to_moments(shours)


if __name__ == "__main__":

    def main():
        """For testing"""
        extractor = MeteoIt()
        extractor.extract()
        print("meteo.it: ", end="")
        print(" - ".join(meteo.states_to_strings(extractor.weather)))

    main()
