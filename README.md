# Analisi Meteo #

Questo progetto traccia le previsioni meteo su alcuni siti/servizi con lo scopo di verificarne la veridicità

### Siti supportati ###

* 3BMeteo
* DarkSky
* Meteo.it
* MeteoAM
* MeteoBlue

### How do I get set up? ###

* Copiare il file `config.py.TEMPLATE` in `config.py`, poi modificare questo file
* Inserire la propria chiave [DarkSky](https://darksky.net/dev/)
* Inserire la configurazione MailGun (url e chiave) per l'invio delle mail
* Creare un progetto su Google e delle [credenziali di tipo service](https://console.developers.google.com/apis/credentials)
* Creare due [Google Spreadsheets](https://docs.google.com/spreadsheets/) e condividerli con l'utente di servizio creato sopra, salvare il file json nella cartella del progetto come `creds.json`